package foxminded;

import java.util.Scanner;

public class Reverse {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		String x;
		
		x = in.nextLine(); //Input your word
		
		System.out.print(x + "=>");
		
		StringBuffer buffer = new StringBuffer(x);
		
		buffer.reverse();
		
		System.out.println(buffer);

	}

}
